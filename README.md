# crud-flb



## Getting started

- What it's done:
A Crud with a monotiltyc application trying to use clean code and design patterns:
  - Files structure: thinking on "this application will grow" (not copy and paste from google), I have created domain folder
  with DTO object in order to avoid blanks, and use it as a transference object inside the Mapper, it is a good practice to Use a mapper on 
  each division, because objects always change, they can be a mix of many sources, or they get a fix during the time.
  (the best Practice is use it with mappers).

  ##  Good Practices:
    - Constructors instead of @Autowired
    - Design patterns such as:
    DTO,DAO,BUILDER,SINGLETON
    - Builders instead of instances
    - MyBatis instead of an ORM such as JPA-Hibernate (if the app grows it can have performance problems in small infrastructures, although it is easier and faster to map automatically when application starts, (I tried, but I had problems with my database privileges , I will explain it bellow )
    - I commented on each controller "@PreaAuthorize", because it is very important to give privileges depending on users roles, without it , there is a vulnerability in code.
    - Logs before and after each functional component, to know when it crashes;
    - Logs without sensitive Information
  ##  Bad Practices:
      -Comments are a bad practice, it is better to have an api definition, or contract, it means problems with pipelines.
      -Incomplete Coverage
      -No stable commits
      -Written code that I am not using (I always try to fix it when I finished unit testing, and optimize imports in a feature)
      -Always Commit to master
      -Relation in tables should be:
      Product oneToMany Images with SKU as FK
      (we should have a Brand Table with its "auto Ids" and Join by SKU , OneToOne with Product))
      -Connection to database credentials (it must come from an environment variable, configured inside the server)
      -Exception handling (I created the class just to show a message but not the complete stacktrace (but I did not implement it yet).
    - Aditional Comments:
    - I Lost time (it was my fault), I had problems with my credit card, and I could not register for free in any Cloud Plataform, 
      I use "MAC OS" and it was difficult to download a local 
      database. I have a remote database, from "Cpanel Host" , and old one that I'd never used, but I can not manage privileges so easily.
    - Maybe with a Test Database or many services I had shown my skills in less time.
    - I will use a new feature branch called "Fix", inside this repo to keep on practicing, if you want to take a look after the deadline).
      Thanks for reading, all comments are welcome
    - For run just open project in Intellij or any other IDE, and run as Spring Boot app with gradle (
      you must have installed gradle, JDK 1.8 JAVA.
      )
    - For run Tests, /gradlew clean build. (now is not working successfully with 85% as needed)
     created By: Diana Sanchez
      
      

      