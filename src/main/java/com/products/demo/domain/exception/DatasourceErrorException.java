package com.products.demo.domain.exception;

public class DatasourceErrorException extends RuntimeException {
    public DatasourceErrorException() {
        super("DatasourceError");
    }
    public DatasourceErrorException(String message) {
        super(message);
    }
}
