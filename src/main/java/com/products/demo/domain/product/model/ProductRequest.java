package com.products.demo.domain.product.model;

import java.util.List;

public class ProductRequest {
    private String Sku;
    private String name;
    private String brand;
    private String size;
    private List<Image> images;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public List<Image> getImages() {
        return images;
    }

    public void setImages(List<Image> images) {
        this.images = images;
    }

    public String getSku() {
        return Sku;
    }

    public void setSku(String sku) {
        Sku = sku;
    }

    public ProductRequest(String sku, String name, String brand, String size, List<Image> images) {
        Sku = sku;
        this.name = name;
        this.brand = brand;
        this.size = size;
        this.images = images;
    }

    public ProductRequest(){}


    public static final class ProductRequestBuilder {
        private String Sku;
        private String name;
        private String brand;
        private String size;
        private List<Image> images;

        private ProductRequestBuilder() {
        }

        public static ProductRequestBuilder aProductRequest() {
            return new ProductRequestBuilder();
        }

        public ProductRequestBuilder withSku(String Sku) {
            this.Sku = Sku;
            return this;
        }

        public ProductRequestBuilder withName(String name) {
            this.name = name;
            return this;
        }

        public ProductRequestBuilder withBrand(String brand) {
            this.brand = brand;
            return this;
        }

        public ProductRequestBuilder withSize(String size) {
            this.size = size;
            return this;
        }

        public ProductRequestBuilder withImages(List<Image> images) {
            this.images = images;
            return this;
        }

        public ProductRequest build() {
            ProductRequest productRequest = new ProductRequest();
            productRequest.setSku(Sku);
            productRequest.setName(name);
            productRequest.setBrand(brand);
            productRequest.setSize(size);
            productRequest.setImages(images);
            return productRequest;
        }
    }
}
