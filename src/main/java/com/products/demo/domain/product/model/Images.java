package com.products.demo.domain.product.model;

import java.util.List;

public class Images {
    List<Image> images;

    public List<Image> getImages() {
        return images;
    }

    public void setImages(List<Image> images) {
        this.images = images;
    }

    public Images(List<Image> images) {
        this.images = images;
    }


}
