package com.products.demo.domain.product.model;

import java.util.List;

public class Product {
    private String Sku;
    private String name;
    private String brand;
    private String size;
    private List<Image> images;

    public Product(String sku, String name, String brand, String size, List<Image> images) {
    }

    public String getSku() {
        return Sku;
    }

    public void setSku(String sku) {
        Sku = sku;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public List<Image> getImages() {
        return images;
    }

    public void setImages(List<Image> images) {
        this.images = images;
    }

    //Builders
    public static ProductBuilder builder(){return new ProductBuilder();}

    public static final class ProductBuilder {
        private String Sku;
        private String name;
        private String brand;
        private String size;
        private List<Image> images;

        private ProductBuilder() {
        }

        public static ProductBuilder aProduct() {
            return new ProductBuilder();
        }

        public ProductBuilder withSku(String Sku) {
            this.Sku = Sku;
            return this;
        }

        public ProductBuilder withName(String name) {
            this.name = name;
            return this;
        }

        public ProductBuilder withBrand(String brand) {
            this.brand = brand;
            return this;
        }

        public ProductBuilder withSize(String size) {
            this.size = size;
            return this;
        }

        public ProductBuilder withImages(List<Image> images) {
            this.images = images;
            return this;
        }

        public Product build() {
           return new Product(Sku,name,brand,size,images);

        }
    }




}
