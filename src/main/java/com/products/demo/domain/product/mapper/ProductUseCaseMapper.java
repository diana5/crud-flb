package com.products.demo.domain.product.mapper;

import com.products.demo.domain.product.model.Product;
import com.products.demo.domain.product.model.Products;
import com.products.demo.resource.repository.product.model.ProductModel;
import com.products.demo.resource.repository.product.model.ProductsModel;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

@Service
public class ProductUseCaseMapper {

    private final ImageModelToImage imageModelToImage;

    public ProductUseCaseMapper(ImageModelToImage imageModelToImage) {
        this.imageModelToImage = imageModelToImage;
    }

    public Product ProductToController(ProductModel product){
        Product.ProductBuilder builder = Product.builder();
        builder.withBrand(product.getBrand());
        builder.withName(product.getName());
        builder.withImages(product.getImages().stream().map(imageModelToImage::mapImage).collect(Collectors.toList())).build();
        builder.withSize(product.getSize());
        builder.withSku(product.getSku());
        return builder.build();

    }


    public Products ProductsToController(ProductsModel products){
        return Products.builder()
                .withProducts(
                        products.getProducts().stream().map(this::ProductToController).collect(Collectors.toList())).build();
    }

}
