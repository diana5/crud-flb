package com.products.demo.domain.product.model;

import java.util.List;

public class Products {
    List<Product> products;

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public Products(List<Product> products) {
        this.products = products;
    }

    //Builders

    public static  ProductsBuilder builder(){return new ProductsBuilder(); }


    public static final class ProductsBuilder {
        List<Product> products;

        private ProductsBuilder() {
        }

        public static ProductsBuilder aProducts() {
            return new ProductsBuilder();
        }

        public ProductsBuilder withProducts(List<Product> products) {
            this.products = products;
            return this;
        }

        public Products build() {
            Products products = new Products(this.products);
            return products;
        }
    }
}
