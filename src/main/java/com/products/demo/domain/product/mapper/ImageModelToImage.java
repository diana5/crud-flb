package com.products.demo.domain.product.mapper;

import com.products.demo.domain.product.model.Image;
import com.products.demo.resource.repository.product.model.ImageModel;
import org.springframework.stereotype.Component;

@Component
public class ImageModelToImage
{
    public Image mapImage(ImageModel image) {
        return Image.builder()
                .withId(image.getId())
                .withUrl(image.getSku())
                .build();
    }
}
