package com.products.demo.domain.product.model;

public class Image {
    private String id;
    private String url;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Image(String id, String url) {
        this.id = id;
        this.url = url;
    }
    public Image() {

    }
    public static ImageBuilder builder() {
        return new ImageBuilder();
    }

    public static final class ImageBuilder {
        private String id;
        private String url;

        //builders

        private ImageBuilder() {
        }



        public ImageBuilder withId(String id) {
            this.id = id;
            return this;
        }

        public ImageBuilder withUrl(String url) {
            this.url = url;
            return this;
        }

        public Image build() {
           return new Image(this.id,this.url);

        }
    }





}
