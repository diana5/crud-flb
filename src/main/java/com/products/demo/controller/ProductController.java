package com.products.demo.controller;

import com.products.demo.domain.product.mapper.ProductUseCaseMapper;
import com.products.demo.domain.product.model.Product;
import com.products.demo.domain.product.model.ProductRequest;
import com.products.demo.domain.product.model.Products;
import com.products.demo.resource.repository.product.ProductRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;


@RestController
public class ProductController {
    Logger logger = LoggerFactory.getLogger(ProductController.class);
    private final ProductUseCaseMapper productUseCaseMapper;
    private final ProductRepository productRepository;

    public ProductController(ProductUseCaseMapper productUseCaseMapper, ProductRepository productRepository) {
        this.productUseCaseMapper = productUseCaseMapper;
        this.productRepository = productRepository;
    }


    @GetMapping(path = "/products")
    // @PreAuthorize("hasRole('A.USER.TYPE')")
    public ResponseEntity<Products> getProducts() {

        logger.info("Entered 'get-products' on ProductController");
        Products response;
        response = productUseCaseMapper.ProductsToController(productRepository.getAllProducts());
        logger.info("Finished 'get-products' on ProductController");
        return ResponseEntity.ok(response);

    }


    @GetMapping(path = "/product/{sku}")
    // @PreAuthorize("hasRole('A.USER.TYPE')")
    public ResponseEntity<Product> getProductBySku(@PathVariable(value = "sku") String sku) {

        logger.info("Entered 'get-product-by-sku' on ProductController");
        Product response;
        response = productUseCaseMapper.ProductToController(productRepository.getProductBySku(sku));
        logger.info("Finished 'get-product-by-sku' on ProductController");
        return ResponseEntity.ok(response);

    }

    @PostMapping(path = "/create")
    // @PreAuthorize("hasRole('A.USER.TYPE')")
    public ResponseEntity<Product> createProduct(@RequestBody ProductRequest product) {

        logger.info("Entered 'create-product' on ProductController");
        productRepository.createProduct(product);
        logger.info("Finished 'create-product' on ProductController");
        return ResponseEntity.created(URI.create("/success")).build();

    }

    @PutMapping(path = "/update/{sku}")
    // @PreAuthorize("hasRole('A.USER.TYPE')")
    public ResponseEntity<Product> updateProduct(String Sku, @RequestBody ProductRequest tutorial) {

        logger.info("Entered 'update-product' on ProductController");
        productRepository.updateProduct(product);
        logger.info("Finished 'update-product' on ProductController");
        return ResponseEntity.ok(null);

    }

    @DeleteMapping(path = "/delete/{sku}")
    // @PreAuthorize("hasRole('A.USER.TYPE')")
    public ResponseEntity deleteProduct(@PathVariable("sku") String sku) {
        logger.info("Entered 'delete-product' on ProductController");
        Product response;
         productRepository.deleteProduct(sku);
        logger.info("Finished 'delete-product' on ProductController");
        return ResponseEntity.ok(null);

    }

}
