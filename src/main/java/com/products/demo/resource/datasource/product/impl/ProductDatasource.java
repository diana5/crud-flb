package com.products.demo.resource.datasource.product.impl;

import com.products.demo.domain.product.model.Product;
import com.products.demo.domain.product.model.ProductRequest;
import com.products.demo.resource.repository.product.model.ProductModel;
import com.products.demo.resource.repository.product.model.ProductsModel;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Mapper
public interface ProductDatasource {
   @Select("select * from product")
    public ProductsModel findAll();

    @Select("SELECT * FROM product WHERE sku = #{sku}")
     public ProductModel findBySku(String sku);

    @Delete("DELETE FROM product WHERE sku = #{sku}")
     public String deleteById(String sku);

    @Insert("INSERT INTO product(sku, name, brand, size) " +
          " VALUES (#{sku}, #{name}, #{brand}, #{size})")
     public String create(ProductRequest product);
}
