package com.products.demo.resource.repository.product.model;

public class ImageModel {
    private String id;
    private String sku;
    private String url;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public ImageModel(){}

    public ImageModel(String id, String sku, String url) {
        this.id = id;
        this.sku = sku;
        this.url = url;
    }



    public static ImageModelBuilder builder(){return new ImageModelBuilder();}

    public static final class ImageModelBuilder {
        private String id;
        private String sku;
        private String url;

        private ImageModelBuilder() {
        }

        public static ImageModelBuilder anImageModel() {
            return new ImageModelBuilder();
        }

        public ImageModelBuilder withId(String id) {
            this.id = id;
            return this;
        }

        public ImageModelBuilder withSku(String sku) {
            this.sku = sku;
            return this;
        }

        public ImageModelBuilder withUrl(String url) {
            this.url = url;
            return this;
        }

        public ImageModel build() {
           return new ImageModel(this.id,this.sku,this.url);

        }
    }
}
