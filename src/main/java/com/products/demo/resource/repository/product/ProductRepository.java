package com.products.demo.resource.repository.product;

import com.products.demo.domain.product.model.ProductRequest;
import com.products.demo.resource.repository.product.model.ProductModel;
import com.products.demo.resource.repository.product.model.ProductsModel;
import org.springframework.http.ResponseEntity;

public interface ProductRepository {
    ProductsModel getAllProducts();
    ProductModel getProductBySku(String sku);
    String createProduct(ProductRequest product);
    String deleteProduct(String Sku);


}
