package com.products.demo.resource.repository.product.model;

import java.util.List;

public class ProductModel {
    private String Sku;
    private String name;
    private String brand;
    private String size;
    private List<ImageModel> images;


    public String getSku() {
        return Sku;
    }

    public void setSku(String sku) {
        Sku = sku;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public List<ImageModel>  getImages() {
        return images;
    }

    public void setImages(List<ImageModel> images) {
        this.images = images;
    }

    public ProductModel(String sku, String name, String brand, String size, List<ImageModel> images) {
        Sku = sku;
        this.name = name;
        this.brand = brand;
        this.size = size;
        this.images = images;
    }

    public ProductModel(){};

//Builders

    public static ProductModelBuilder builder(){return new ProductModelBuilder();}


    public static final class ProductModelBuilder {
        private String sku;
        private String name;
        private String brand;
        private String size;
        private List<ImageModel> images;

        private ProductModelBuilder() {
        }

        public static ProductModelBuilder aProductModel() {
            return new ProductModelBuilder();
        }

        public ProductModelBuilder withSku(String sku) {
            this.sku = sku;
            return this;
        }

        public ProductModelBuilder withName(String name) {
            this.name = name;
            return this;
        }

        public ProductModelBuilder withBrand(String brand) {
            this.brand = brand;
            return this;
        }

        public ProductModelBuilder withSize(String size) {
            this.size = size;
            return this;
        }

        public ProductModelBuilder withImages(List<ImageModel> images) {
            this.images = images;
            return this;
        }


        public ProductModel build() {
            return new ProductModel(sku,name, brand, size, images);

        }
    }
}


