package com.products.demo.resource.repository.product.model;

import java.util.List;

public class ProductsModel {
    List<ProductModel> products;

    public List<ProductModel> getProducts() {
        return products;
    }

    public void setProducts(List<ProductModel> products) {
        this.products = products;
    }

    public ProductsModel(List<ProductModel> products) {
        this.products = products;
    }

    public ProductsModel() {
    }


    public static ProductsModelBuilder builder(){return new ProductsModelBuilder();}


    public static final class ProductsModelBuilder {
        List<ProductModel> products;

        private ProductsModelBuilder() {
        }

        public static ProductsModelBuilder aProductsModel() {
            return new ProductsModelBuilder();
        }

        public ProductsModelBuilder withProducts(List<ProductModel> products) {
            this.products = products;
            return this;
        }


        public ProductsModel build() {
            ProductsModel productsModel = new ProductsModel(this.products);
            return productsModel;
        }
    }
}
