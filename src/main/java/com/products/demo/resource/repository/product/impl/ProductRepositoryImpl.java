package com.products.demo.resource.repository.product.impl;

import com.products.demo.domain.product.model.ProductRequest;
import com.products.demo.resource.datasource.product.impl.ProductDatasource;
import com.products.demo.resource.repository.product.ProductRepository;
import com.products.demo.resource.repository.product.model.ProductModel;
import com.products.demo.resource.repository.product.model.ProductsModel;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;

@Repository
public class ProductRepositoryImpl implements ProductRepository
{

    private final ProductDatasource productDatasource;

    public ProductRepositoryImpl(ProductDatasource productDatasource) {
        this.productDatasource = productDatasource;
    }

    @Override
    public ProductsModel getAllProducts() {
        return productDatasource.findAll();
    }

    @Override
    public ProductModel getProductBySku(String sku) {
        return productDatasource.findBySku(sku);
    }

    @Override
    public String createProduct(ProductRequest product) {
        return productDatasource.create(product);

    }

    @Override
    public String deleteProduct(String Sku) {
        return productDatasource.deleteById(Sku);
    }


}
