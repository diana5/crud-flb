package com.products.demo.domain.controller;

import com.products.demo.controller.ProductController;
import com.products.demo.domain.product.mapper.ProductUseCaseMapper;
import com.products.demo.domain.product.model.Product;
import com.products.demo.mock.ProductMock;
import com.products.demo.resource.repository.product.ProductRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@ExtendWith(MockitoExtension.class)
public class ProductControllerTest {
    private static final String SKU = "23435K-35F";
    private ProductController productController;
    @Mock
    private ProductRepository productRepository;
    @Mock
    private ProductUseCaseMapper productUseCaseMapper;

    @BeforeEach
    void init() {
        ProductController productController = new ProductController(
                productUseCaseMapper,
                productRepository);
    }

    @Test
    @DisplayName("SHOULD return HttpStatus.OK WHEN Repository product called GIVEN a SKU-PRODUCT")
    void getProductTest() {

        ResponseEntity<Product> productActual = new ResponseEntity<>(
                ProductMock.buildSuccess(), HttpStatus.OK);

        ResponseEntity<Product> expectedResponse = productController
                .getProductBySku(SKU);

        assertThat(expectedResponse.getStatusCode())
                .isEqualTo(productActual.getStatusCode());
    }

}
