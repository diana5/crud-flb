package com.products.demo.mock;

import com.products.demo.domain.product.model.Image;
import com.products.demo.domain.product.model.Product;

import java.util.ArrayList;
import java.util.List;

public class ProductMock {
    public static Product buildSuccess(){
        List<Image> images = new ArrayList<>();
        Image image = new Image();
        image.setId("1");
        image.setUrl("https://falabella.scene7.com/is/\n" +
                "image/Falabella/881898502_1");
        images.add(image);
        return Product.builder().withName("Zapatilla Cuero")
                .withSku("#42-12JDK")
                .withBrand("nike")
                .withSize("S")
                .withImages(images).build();
    }
}
